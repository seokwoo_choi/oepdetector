# README #

OEPDetector is a pin tool to get an OEP(original entry point) from packed x86-64 PE file. 
It can find near OEP of Themida, VMProtect, UPX packed files. 

### Algorithm
OEPDetector keeps memory writes and check whether the written address is executed. 
OEP is one of the written and executed addresses. 


### Build
Requires Intel Pin 3.0 and Visual Studio 2015  

### Test 
After building a dll file run with pin. Generated text file contains OEP candidates. 

~~~ 
pin -t c:\pintool\OEPDetector32.dll -- HWCV.tmd.exe
~~~