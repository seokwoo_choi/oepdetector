// CV Analyzer 
// Author: seogu.choi@gmail.com
// Date: 2015.4.25. ~ 

#include "OEPDetector.h"
#include "StrUtil.h"
#include <string>

KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE, "pintool", "o", "result.txt", "specify file name for the result");

// Trace hook
void TRC_analysis(ADDRINT addr, THREADID tid) {

	// check only main thread
	if (tid != 0) return;

	// check only text section of main executable
	if (addr >= main_txt_eaddr || addr < main_txt_saddr) return;

	// check oep: written and executed
	// written and executed address is made clear to prove overlapped write and execute
	if (page_written_count.find(addr / 4096) != page_written_count.end()) {
		page_written_count.clear();
		*fout << "OEP:" << toHex(addr) << endl;
	}	
}

// memory write hook
void INS_MW_analysis(CONTEXT *ctxt, ADDRINT iaddr, size_t size, ADDRINT waddr,THREADID tid) {
	// record memory write address
	page_written_count[waddr / 4096]++;
}

// Trace instrumentation 
// Hook memory read/write instruction and record such addresses. 
// Check execute address whether it is written. 
void TRC_inst(TRACE trc, void *v) {
	ADDRINT addr = TRACE_Address(trc);

	// check only main image
	if (addr >= main_img_eaddr || addr < main_img_saddr) return;

	// hook only text section of main executable
	if (addr >= main_txt_saddr && addr < main_txt_eaddr) {
		TRACE_InsertCall(trc, IPOINT_BEFORE, (AFUNPTR)TRC_analysis,
			IARG_ADDRINT, addr,
			IARG_THREAD_ID,
			IARG_END);
	}

	for (BBL bbl = TRACE_BblHead(trc); BBL_Valid(bbl); bbl = BBL_Next(bbl))
	{
		for (INS ins = BBL_InsHead(bbl); INS_Valid(ins); ins = INS_Next(ins))
		{
			ADDRINT insaddr = INS_Address(ins);			

			// record memory write excluding stack operation
			if (INS_IsMemoryWrite(ins) && !INS_IsStackWrite(ins))
			{
				INS_InsertPredicatedCall(
					ins, IPOINT_BEFORE, (AFUNPTR)INS_MW_analysis,
					IARG_CONTEXT,
					IARG_INST_PTR,
					IARG_MEMORYWRITE_SIZE,
					IARG_MEMORYWRITE_EA,
					IARG_THREAD_ID,
					IARG_END);
			}
		}
	}
}

// Image instrumentation
// Get symbol information when a pe file is loaded. 
void IMG_inst(IMG img, void *v)
{
	string imgname = IMG_Name(img);

	size_t pos = imgname.rfind("\\") + 1;
	imgname = imgname.substr(pos);

	mod_info_t *dllinfo = NULL;

	if (module_info_m.find(imgname) == module_info_m.end())
	{
		string name = imgname;
		ADDRINT saddr = IMG_LowAddress(img);
		ADDRINT eaddr = IMG_HighAddress(img);

		dllinfo = new mod_info_t(name, saddr, eaddr);
		module_info_m[name] = dllinfo;
			
		if (IMG_IsMainExecutable(img))
		{			
			*fout << "IMAGE:" << imgname << "[" << toHex(saddr) << ',' << toHex(eaddr) << ']' << endl;
			main_img_saddr = saddr;
			main_img_eaddr = eaddr;

			aslr_diff = main_img_saddr - 0x400000;

			SEC sec = IMG_SecHead(img);

			main_txt_saddr = SEC_Address(sec);
			main_txt_eaddr = main_txt_saddr + SEC_Size(sec);
		}
	}
	else
	{
		// no symbol information
		return;
	}

	for (SEC sec = IMG_SecHead(img); SEC_Valid(sec); sec = SEC_Next(sec))
	{
		string secname = SEC_Name(sec);
		ADDRINT saddr = SEC_Address(sec);
		ADDRINT eaddr = saddr + SEC_Size(sec);
		sec_info_t *secinfo = new sec_info_t(imgname, secname, saddr, eaddr);
		dllinfo->sec_infos.push_back(secinfo);
		
		if (IMG_IsMainExecutable(img)) *fout << "SECTION:" << *secinfo << endl;

		if (SEC_Name(sec) == ".text")
		{

			if (IMG_IsMainExecutable(img))
			{
				main_txt_saddr = SEC_Address(sec);
				main_txt_eaddr = main_txt_saddr + SEC_Size(sec);
			}

			for (RTN rtn = SEC_RtnHead(sec); RTN_Valid(rtn); rtn = RTN_Next(rtn))
			{
				string rtnname = RTN_Name(rtn);
				ADDRINT saddr = RTN_Address(rtn);
				ADDRINT eaddr = saddr + RTN_Range(rtn);
				fn_info_t *fninfo = new fn_info_t(imgname, rtnname, saddr, eaddr);

				fn_info_m[saddr] = fninfo;
				module_info_m[imgname]->fn_infos.push_back(fninfo);
			}
		}
	}
}

// This routine is executed every time a thread is created.
VOID ThreadStart(THREADID threadid, CONTEXT *ctxt, INT32 flags, VOID *v)
{
	*fout << "Starting Thread " << threadid << endl;
}

// This routine is executed every time a thread is destroyed.
VOID ThreadFini(THREADID threadid, const CONTEXT *ctxt, INT32 code, VOID *v)
{
	*fout << "Ending Thread " << threadid << endl;
}

/*!
* Print out analysis results.
* This function is called when the application exits.
* @param[in]   code            exit code of the application
* @param[in]   v               value specified by the tool in the
*                              PIN_AddFiniFunction function call
*/
void Fini(INT32 code, void *v)
{
	((ofstream*)fout)->close();
}

/*!
* The main procedure of the tool.
* This function is called when the application image is loaded but not yet started.
* @param[in]   argc            total number of elements in the argv array
* @param[in]   argv            array of command line arguments,
*                              including pin -t <toolname> -- ...
*/
int main(int argc, char *argv[])
{
	// Initialize PIN library. Print help message if -h(elp) is specified
	// in the command line or the command line is invalid 
	if (PIN_Init(argc, argv))
	{
		return -1;
	}
	string outputFileName = KnobOutputFile.Value();
	fout = new ofstream(outputFileName.c_str());


	PIN_InitSymbols();

	// Register Analysis routines to be called when a thread begins/ends
	PIN_AddThreadStartFunction(ThreadStart, 0);
	PIN_AddThreadFiniFunction(ThreadFini, 0);

	IMG_AddInstrumentFunction(IMG_inst, 0);	
	TRACE_AddInstrumentFunction(TRC_inst, 0);

	// Register function to be called when the application exits
	PIN_AddFiniFunction(Fini, 0);

	// Start the program, never returns
	PIN_StartProgram();

	return 0;
}

/* ===================================================================== */
/* eof */
/* ===================================================================== */
