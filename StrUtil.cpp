#include "StrUtil.h"


string UpperCase(string s)
{
	int dif = 'a' - 'A';
	for (size_t i = 0;i <s.length();i++)
	{
		if ((s[i] >= 'a') && (s[i] <= 'z'))
			s[i] -= dif;
	}
	return s;
}

string LowerCase(string s)
{
	int dif = 'a' - 'A';
	for (size_t i = 0;i<s.length();i++)
	{
		if ((s[i] >= 'A') && (s[i] <= 'Z'))
			s[i] += dif;
	}
	return s;
}
