// CV Analyzer 
// 2015.04.25. 
// seogu.choi@gmail.com

#include "pin.H"
#include "PinSymbolInfoUtil.h"

#include <iostream>
#include <fstream>
#include <map>
#include <set>

#define MAX_THREAD 50


// OEP finding
map<ADDRINT, size_t> page_written_count;

// lock serializes access to the output file.
PIN_LOCK lock;

// standard output & file output 
ostream * fout = &cerr;	// result output


// obfuscated module information
ADDRINT main_img_saddr = 0;	// section start address where EIP is changed into 
ADDRINT main_img_eaddr = 0;
ADDRINT main_txt_saddr = 0;	// section start address where EIP is changed into 
ADDRINT main_txt_eaddr = 0;

// ASLR check
ADDRDELTA aslr_diff;

// region info 
vector<reg_info_t*> region_info_v;

// module info 
map<string, mod_info_t*> module_info_m;

// function info
map<ADDRINT, fn_info_t*> fn_info_m;

// hooking function
void TRC_analysis(ADDRINT addr, THREADID tid);
void INS_MW_analysis(CONTEXT *ctxt, ADDRINT iaddr, size_t size, ADDRINT waddr, THREADID tid);
void TRC_inst(TRACE trc, void *v);
void IMG_inst(IMG img, void *v);
